#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:709abb9dcb3a936900ae205c701e2986eb082baf; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:24d09513c7cbc6cf5f89db1bdc493609667a15e5 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:709abb9dcb3a936900ae205c701e2986eb082baf && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
